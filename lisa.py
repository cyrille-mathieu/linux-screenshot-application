#!/bin/python3

# Date: 2021/05/07

# Copyright © 2021 Cyrille Mathieu

# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import os
import time
from datetime import datetime as dt

number_of_screenshots = 0

# Ask parameters

print("Linux Screenshot Application")
print("")
student_first_name = str.lower(input("Enter your first name: "))
student_last_name = str.lower(input("Enter your last name: "))
student_full_name = str(student_first_name + "_" + student_last_name).replace(" ", "_")
run_duration = int(input("Enter the duration for the script (minutes): "))


# Screenshot logic

student_screenshot_dir = str(student_full_name)

if not os.path.isdir("./" + student_screenshot_dir):
	os.mkdir(student_screenshot_dir)
	os.chdir("./" + student_screenshot_dir)
else:
	os.chdir("./" + student_screenshot_dir)

def take_screenshot():
	today = dt.today()
	file_name = str(student_full_name + "_" + str(today.year) + "_" + str(today.month) + "_" + str(today.day) + "_" + str(today.hour) + "_" + str(today.minute) + "_" + str(today.second) + ".png")
	os.system("import -silent -window root " + file_name)
	print("Saving " + file_name)


# Timer loop, run every 60 seconds

start_time = time.time()

while number_of_screenshots < run_duration:
	take_screenshot()
	number_of_screenshots += 1
	time.sleep(60.0 - (time.time() - start_time) % 60.0)


print("")
print("++++++++++++++++++++++++++++++++++++++++++++++++++++++")
print("+                                                    +")
print("+  Don't forget to zip the directory and upload it!  +")
print("+                                                    +")
print("++++++++++++++++++++++++++++++++++++++++++++++++++++++")
