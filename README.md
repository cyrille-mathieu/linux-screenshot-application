# Linux Screenshot Application

## How it works / origin

Linux Screenshot Application (LISA for short) is an alternative to (the now defunct) [HOSA](https://hosa.toegepasteinformatica.be) and can be used in situations where traditional proctoring solutions for exams are not feasable.  
LISA does exactly the same, except on GNU/Linux. It asks for the first and last name(s) of the student and the desired duration for the script to run.  
LISA then makes a screenshot every 60 seconds, and saves all screenshots with a name and timestamp.

## Requirements

- Python 3.9 or higher.
- `ImageMagick`, install it for:
  - Debian Systems: `sudo apt install imagemagick`
  - RHEL/Fedora Systems: `sudo dnf install ImageMagick`
- X Server

## Installation

Copy and paste the following line into your favourite terminal:

> `wget https://gitlab.com/cyrille-mathieu/linux-screenshot-application/-/raw/master/lisa.py && chmod 744 ./lisa.py`

## Running LISA

> `python3 ./lisa.py`
